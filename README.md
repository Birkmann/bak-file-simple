# Backup

A file based backup tool, focused on simplicity and reusablity of backup-files.

## Installation

- $ git clone https://gitlab.com/Birkmann/bak-file-simple
- cd bak-file-simple
- stack install

## File format
A bak-file consists of one or more trees. A tree is built from **nodes** which themselves contain again **actions** and other nodes.
Thats it.

## IMPORTANT Design choices
- **WARNING:**: As of yet _identifiers may not contain any whitespace and there is no workaround_. This is deliberate as i do not like whitespace in identifiers. This will change in the future though. If you try to use whitespace in identifiers you will get cryptic parser errors. 
- All paths must be absolute or the program must be able to resolve them to an absolute path. See Syntax on Actions.
## Syntax

### Nodes
Node identifiers are strings _without_ whitespace characters, so `hello` and `skdfj#23487,LJL←↓→↑` are valid identifiers, but `hello world`, `tabs \t and \n newlines` (with `\t` a tab character and `\n` a newline) are not valid node identifiers. Nodes may set a _base directory_ via `node_identifier <- /path/to/base/directory`. Base paths must be absolute, but can be concatenated, i.e. instead of writing 
```
node1 <- /etc/nginx
    node2 <- /etc/nginx/sites-available
``` 
you can write
```
node1 <- /etc/nginx
    node2 <- sites-available
``` 
Contents of a node must be indented.

### Actions
Actions are paths prefixed by a one-character `modifier` e.g. `+ exec`, sometimes with additional info. The simplest modifier is `+`, others will be explained below. The path may not contain whitespace. Paths must be either absolute or resolvable to an absolute path via the current base directory.
So 
```
nginx
    + /etc/nginx/sites-available
```
is ok,
```
nginx <- /etc/nginx
    + sites-available
```
is ok, but
```
nginx
    + sites-available
```
is NOT ok even if your current working directory is `/etc/nginx`.

## Simple example
Lets look at a simple example of a tree:
```
config 

    # backup of common bash files we do not want to lose
    + .bashrc
    + .bash_aliases

    # backup of common emacs files
    emacs <- ~/.emacs.d
        + init.el
        + snippets
```