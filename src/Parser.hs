{-# LANGUAGE ScopedTypeVariables #-}
module Parser where

-- import Prelude hiding (fail)
import Protolude hiding ((%))
import qualified Text.Megaparsec.Char.Lexer as L
import Text.Megaparsec
import qualified Text.Megaparsec as M
import qualified Text.Megaparsec.Char as MC
import qualified Data.Char as C
import qualified Data.Text as T
import Text.Regex
import Optics

import Control.Monad.Fail

import BakTree
import PathHelper


type Parser = ParsecT Void T.Text IO




-----------
-- Lexer --
-----------

spaceNoNewline :: Parser ()
spaceNoNewline = void $ MC.char ' ' <|> MC.char '\t'

scn, sc :: Parser ()
scn = L.space MC.space1 (L.skipLineComment "#") empty
sc = L.space spaceNoNewline (L.skipLineComment "#") empty

mustWhitespace :: Parser ()
mustWhitespace = void $ M.some spaceNoNewline 

lexeme :: Parser a -> Parser a
lexeme = L.lexeme sc

seperator :: T.Text -> Parser ()
seperator s = MC.string' s  >> mustWhitespace

lexemeWithPos :: Parser a -> (a -> Bool) -> T.Text -> Parser a
lexemeWithPos p checker msg = do
    erg <- p
    pos <- getOffset  
    when (checker erg) (throwErrSourcePos pos msg)
    sc
    return erg

throwErrSourcePos :: Int -> T.Text -> Parser b
throwErrSourcePos offset msg = setOffset (offset-1) >> fail (T.unpack msg)





symbol :: T.Text -> Parser T.Text
symbol = L.symbol sc

symbol_ :: T.Text -> Parser ()
symbol_ s = void $ symbol s 

-- modifier symbols
plus_, link_, par_ :: Parser ()
[plus_, link_, par_] = symbol_ <$> [ "+" , "*", "&"]

betweenSlashes :: Parser a -> Parser a
betweenSlashes = between (symbol_ "/") (symbol_ "/")

noSpace :: Parser T.Text
noSpace = takeWhile1P (Just "no space") (not . C.isSpace)


------------
-- Parser --
------------

dirSegmentP :: Parser T.Text
dirSegmentP = takeWhile1P (Just "no slash and no space") (\c -> c /= '/' && not (C.isSpace c))

pathRawP :: Parser T.Text
pathRawP = lexeme noSpace <?> "path"

inputPathP :: Parser InputPath
inputPathP = do
    liftA2 InputPath
                    ( sanitizePath <$> lexeme noSpace ) 
                    $ regexP <|> newNameP <|> runCommandP <|> noOptionsP

-- parser of path options
regexP, newNameP, runCommandP, noOptionsP :: Parser PathOptions


regexP =  do
    seperator "match"
    regexInput <- lexeme noSpace
    let compiledRegexMayErr = mkRegex . T.unpack $ regexInput
    mayRegex <- liftIO $ (Just <$> evaluate compiledRegexMayErr) 
                         `catch` (\(_ :: IOException) -> return Nothing) -- catch the ioexception thrown by `mkRegex`
    case mayRegex of 
        Nothing -> fail $ "Not a valid regular expression: " <> T.unpack regexInput
        Just regex -> return $ Regex regex
            
newNameP  = fmap NewName $ seperator "as" *> lexeme dirSegmentP
  

runCommandP = RunCommand 
    <$> ( seperator ">>>" >> lexeme dirSegmentP )
    <*> ( seperator "<<<" >> takeWhile1P (Just "no newline") (/= '\n'))

noOptionsP = return NoOptions
  
        



modifierP :: Parser Modifier
modifierP 
    =   Plus  <$ plus_ 
    <|> Link  <$ link_  
    <|> Par   <$ par_
    <?> "modifier"

dirLevelP :: Parser [T.Text]
dirLevelP = (lexeme $ dirSegmentP `sepBy1` (MC.char '/')) <?> "directory node"

-- | Computing a single backup tree
-- the argument specifies whether a base path has been given
dirTreeP :: Bool -> Parser BakTree
dirTreeP bps = L.indentBlock scn $ do
            levelSegments <- dirLevelP -- level header
            maybeBaseChange <- changeBaseP -- base change
            let -- pathNodeP actually depends on `maybeBaseChange` because a base path might have been set, otherwise err of not bps
                pathNodeP = do
                        mods <- modifierP
                        offset <- getOffset
                        inp <- inputPathP
                        errIfRelAndBaseNotSet 
                            offset
                            (isJust maybeBaseChange || bps) -- header might have been set in maybeBaseChange, or was set before
                            (inp ^. path)
                        return $ PathNode inp mods
                
            return $ L.IndentMany Nothing 
                        (\items -> return $ BakTree 
                                            levelSegments
                                            maybeBaseChange
                                            (lefts items) 
                                            (rights items))
                        (eitherP  -- every node is either
                            pathNodeP -- a path node
                            ( dirTreeP $ isJust maybeBaseChange || bps) -- or a recurive directory tree itself
                            <?> "directory node or input path")
                    
    where 
        -- parses the optional changing of the base path
        changeBaseP = do
            offAndMayBase <- optional $ (symbol_ "<-" <|> symbol_ "←") *> ((,) <$> getOffset <*> pathRawP) -- (change of) base directory, take offset for pretty error
            let newBase = (fmap . fmap) sanitizePath $ offAndMayBase :: Maybe (Int, Path)
            maybe (pure ()) (\(off, p) -> errIfRelAndBaseNotSet (off+2) bps p) $ newBase
            pure . fmap snd $ newBase

        -- checks that a base path is set if encountering a relative input path
        errIfRelAndBaseNotSet offset baseSet p = 
            when ( isJust (p ^? base % _Rel) -- base path is relative
                    && not baseSet ) $              -- but the basePath is not set
                (throwErrSourcePos offset $ "cannot use a relative path segment if base path is not set: " <> p ^. rawPath)
        
        
        
-- | parses a backup file
parser :: Parser [BakTree]
parser = M.many (L.nonIndented scn $ dirTreeP False) <* eof                        

