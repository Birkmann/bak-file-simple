{-# LANGUAGE ScopedTypeVariables #-}
module CLI where

import Optics
import Protolude hiding ((%))
import Prelude(read)
import Options.Applicative
import qualified Data.Text as T
import Text.Printf
import BakTree
import PathHelper
import Parser(parser)
import Text.Megaparsec(runParserT, errorBundlePretty)
import Data.Time
import System.Environment
import System.IO.Error(IOError, isDoesNotExistError)
import Data.Text.Prettyprint.Doc.Util(putDocW)
import Data.Text.Prettyprint.Doc
import Control.Monad.Logger


runBackupWithConfig :: Config -> IO ()
runBackupWithConfig userConfig@Config{..} = do
    destDir <- sanitizePath  configOutputDir `existsAndIs` IsDir
    bakFile <- sanitizePath configBackupFile `existsAndIs` IsFile
    let strBakFile = T.unpack $ bakFile ^. raw
    trees <-  readFile strBakFile
          >>= runParserT parser strBakFile 
          >>= either
                    (die . T.pack . errorBundlePretty) -- die on error
                    return

    let traverseTree :: Config -> BakTree -> IO ()
        traverseTree config
            | configVerbose = runStdoutLoggingT 
                            . flip runReaderT (configToTraverseOptions config) 
                            . simpleTraverser destDir
            | otherwise     = runNoLoggingT 
                            . flip runReaderT (configToTraverseOptions config) 
                            . simpleTraverser destDir
        runBackup :: Config -> [BakTree] -> IO ()
        runBackup config@Config{configDryRun = dry} bakTrees = do
            prettyPrintToTerminal config
            startTime <- getCurrentTime
            for_ bakTrees (traverseTree config) 
                `catch`  \(err :: TraverseError) -> dieWithMsg err
            endTime <- getCurrentTime
            unless dry $ printf "Backup took %s seconds\n" $ showt $ diffUTCTime endTime startTime
                
        
        doNotUnderstandLoop :: IO ()
        doNotUnderstandLoop = do
            printf "\n------------------------------------------------------\n"
            printf "The backup file seems OK. Do you want to run it? [Y/n]\n"
            input <- getLine 
            if 
                | input `elem` ["Y", "y", "Yes", "yes", "YES", "", "\n"] -> 
                    runBackup userConfig{configDryRun=False} trees
                | input `elem` ["N", "n", "No", "no", "NO"] -> 
                    printf "Ok. \nBye.\n"
                | otherwise -> do
                    printf "I could not understand input %s\n" input
                    doNotUnderstandLoop

    _ <- runBackup userConfig trees
    when configDryRun doNotUnderstandLoop -- if dry run, ask if user wants to run it for real
                            


--------------------------------
-- # IO helper
--------------------------------

defaultTerminalWidth :: Int
defaultTerminalWidth = 80

prettyPrintToTerminal :: Pretty a => a -> IO ()
prettyPrintToTerminal p = do
    terminalWidth <- ( read @Int <$> getEnv "COLUMNS" ) `catch` 
        (\(e :: IOError) -> if isDoesNotExistError e
                                then return defaultTerminalWidth
                                else throwIO e )
    putDocW terminalWidth . pretty $p
    


existsAndIs :: Path -> PathKind -> IO Path
existsAndIs p kind = do
    unlessM (pathIsExist p) $ dieTraverseErr p PathDoesNotExist
    realPath <- pathIsLink p >>= \case 
                    True ->  resolveLink p
                    False -> return p
    case kind of
        IsFile -> unlessM (pathIsFile realPath) $ dieTraverseErr realPath NotAFile
        IsDir -> unlessM (pathIsDir realPath) $ dieTraverseErr realPath NotADirectory
        _ -> panic . T.pack $ printf "should not happen: kind is %s\n" (showt kind)
    mkAbs realPath
    where
        dieTraverseErr :: Path -> TraverseErrorType -> IO b
        dieTraverseErr p' e = dieWithMsg $ TraverseError p' e Nothing



dieWithMsg :: Pretty a => a -> IO b
dieWithMsg msg = do
    prettyPrintToTerminal msg
    die "Aborting."



-------------------------------
-- | # CLI Parser
-------------------------------


-- | The input options
data Config = Config
  { configOutputDir :: T.Text
  , configDryRun :: Bool
  , configVerbose :: Bool
  , configRestore :: Bool
  , configIgnoreLinks :: IgnoreLinks
  , configKeepGoing :: Bool
  , configBackupFile :: T.Text
  } deriving (Show, Eq)

configToTraverseOptions :: Config -> TraverseOptions   
configToTraverseOptions Config{..} = 
        TraverseOptions 
            { ignoreLinks = configIgnoreLinks
            , dryRun = configDryRun
            , keepGoing = configKeepGoing
            , restore = configRestore
            }

instance Pretty Config where
    pretty Config{..} = vsep
        ["# -- Configuration -- #"
        , indent 2 . vsep . fmap ("-" <+>) $
            [ "Backup file" <> colon <+> pretty configBackupFile
            , "Output directory" <> colon <+> pretty configOutputDir
            , vsep 
                [ "Options:" 
                , indent 2 . vsep . fmap ("*" <+>) $ 
                    [ "dry run" <> colon <+> pretty configDryRun
                    , "restore" <> colon <+> pretty configRestore
                    , "verbose" <> colon <+> pretty configVerbose
                    , "links to ignore" <> colon <+>  pretty configIgnoreLinks
                    , "keep going" <> colon <+> pretty configKeepGoing
                    ]
                ]
            ]
        ] <> line


parseUserOpts :: IO Config
parseUserOpts = execParser opts

opts :: ParserInfo Config
opts = info (parseConfig <**> helper)
      ( fullDesc
     <> progDesc "Backup the files specified in BACKUP-FILE to DIRECTORY"
     <> header   "backup - a simple file-based backup tool" )

parseRestore :: Parser T.Text
parseRestore = argument str 
                (  metavar "BACKUP-FILE"
                <> help "Backup file to restore" 
                )

parseConfig :: Parser Config  
parseConfig = Config
                <$> strOption
                    (  long "output-dir"
                    <> short 'O'
                    <> metavar "DIRECTORY"
                    <> help "Directory to copy the files into" )
                <*> switch
                    (  long "dry-run"
                    <> short 'D'
                    <> help "Only check if the actions can be executed successfully" )
                <*> switch
                    (  long "verbose"
                    <> short 'V'
                    <> help "Print actions verbosely" )
                <*> switch
                    (  long "restore"
                    <> short 'R'
                    <> help "Restore the backup file" )
                <*> Options.Applicative.option (maybeReader $ 
                        readMaybe @IgnoreLinks . mappend "Ignore" )
                    (  long "ignore-links"
                    <> value (ignoreLinks defaultTraverseOptions)
                    <> metavar "TYPE"
                    <> showDefaultWith (drop 6 . show)
                    <> help "Ignore Links: [All|None|Broken]" )
                <*> switch
                    (  long "keep-going"
                    <> help "Ignore errors" )
                <*> argument str (metavar "BACKUP-FILE")
    
                    
                    
                    
                    
