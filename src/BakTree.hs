{-# LANGUAGE 
  TemplateHaskell
, BangPatterns
, NoMonomorphismRestriction
, FlexibleContexts
, ConstraintKinds
, ScopedTypeVariables
, DeriveDataTypeable
, RankNTypes
#-}
module BakTree where

import Prelude(String)
import Protolude hiding ((%), handle, throwIO, log, catch, catchJust, handleJust, tryIO)
import qualified Data.Text as T
import Text.Regex(matchRegex, Regex)
import Optics.TH
import Optics
import Control.Monad.IO.Unlift(MonadUnliftIO, withRunInIO)
import UnliftIO.Exception
import Control.Concurrent.ParallelIO(parallel_)
import System.IO.Error
import PathHelper
import UnliftIO.Directory
import System.Directory(createFileLink, createDirectoryLink)
import Data.Text.Prettyprint.Doc
import Text.Printf
import Control.Monad.Logger
import qualified System.Process as Process



-- ############################################
-- ####  BakTree
-- ############################################

-- | Invariant: If any inputPath in the tree (baseDir oder in pathNode) is relativ then there was an absolute inputPath given before
data BakTree = BakTree
    { _dirName :: [T.Text] -- ^ the new subdirectory of the backuptree
    , _srcBaseDir :: !(Maybe Path) -- ^ the current base directory, if it exists
    , _pathNodes :: [PathNode] -- ^ the inodes to be inserted in this subdirectory
    , _subTrees :: [BakTree] -- ^ the subtrees
    } deriving (Show)
makeLenses ''BakTree


-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
-- ############################################
-- ####  Traversery Helper
-- ############################################
-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




-- ############################################
-- ####  Error
-- ############################################

-- | Possible Errors
data TraverseErrorType
  = PathDoesNotExist 
  | NotAFile
  | NotADirectory 
  | DirectoryAlreadyExists 
  | BrokenLink
  | PermissionDenied
  | InternalError
  deriving (Show, Eq, Enum, Typeable)

instance Pretty TraverseErrorType where
  pretty = \case
    PathDoesNotExist  -> "Path does not exist"
    NotAFile  -> "Not a file"
    NotADirectory  -> "Not a directory"
    DirectoryAlreadyExists -> "Directory already exsists"
    BrokenLink -> "Broken link"
    PermissionDenied -> "Permission denied"
    InternalError -> "Internal error"


-- | Error type
data TraverseError = TraverseError  
  { errPath :: Path -- ^ path on which the error occurs
  , errType :: TraverseErrorType
  , errorContext :: Maybe T.Text -- ^ optional message
  } deriving (Show, Eq, Typeable)

instance Exception TraverseError where
  displayException = show . pretty


traverseErr :: MonadIO m => Path ->  TraverseErrorType -> m a
traverseErr p t = throwIO $ TraverseError p t Nothing

traverseErrMsg :: MonadIO m => Path -> TraverseErrorType -> T.Text -> m a
traverseErrMsg p t msg = throwIO $ TraverseError p t (Just msg)

instance Pretty TraverseError where
  pretty TraverseError{..} = vsep 
    [ hsep [ "[ERROR]" <> colon <+>  pretty errType]
    , indent 2 . vsep $
        [ "on" <+> pretty errPath
        , pretty $ mappend "Reason: " <$> errorContext
        ]
    ]


-- ############################################
-- ####  Options
-- ############################################

-- | Options
data TraverseOptions = TraverseOptions
  { ignoreLinks :: IgnoreLinks
  , dryRun :: Bool -- ^ does not check directories recursively
  , keepGoing :: Bool
  , restore :: Bool
  } deriving(Show, Eq)


ignoreAllLinks, ignoreNoneLinks, ignoreBrokenLinks :: TraverseOptions -> Bool
ignoreAllLinks = (== IgnoreAll) . ignoreLinks
ignoreNoneLinks = (== IgnoreNone) . ignoreLinks
ignoreBrokenLinks = (== IgnoreBroken) . ignoreLinks


-- | How to deal with links
data IgnoreLinks = IgnoreAll | IgnoreNone | IgnoreBroken deriving (Show, Eq, Ord, Enum, Read)

instance Pretty IgnoreLinks where
  pretty = pretty . T.drop 6 . showt

defaultTraverseOptions :: TraverseOptions
defaultTraverseOptions = TraverseOptions
 { ignoreLinks = IgnoreBroken, dryRun = False, keepGoing=False, restore=False }





-- ############################################
-- ####  Logging
-- ############################################

log :: (MonadLogger m, Pretty a) => T.Text -> T.Text -> Maybe a -> m ()
log !typ !msg mayPath = 
  logOtherN 
    (LevelOther typ) 
    ( show . (<> line) . (pretty msg <+>). pretty $ mayPath )

logCopy :: (MonadLogger m, MonadIO m) => Path -> Path -> m ()
logCopy src dest = fileKind src >>= \case
  IsDir -> logOtherN (LevelOther "copy dir") $ show $ "copying directory" <+> pretty src <+> "recursively to" <+> pretty dest
  IsFile -> logOtherN (LevelOther "copy file") $ show $ "copying file" <+> pretty src <+> "to" <+> pretty dest
  _ -> pure ()





-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
-- ############################################
-- ####  Traverser
-- ############################################
-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



-- ############################################
-- ####  Traversing Type
-- ############################################

type OptsIO m = (MonadReader TraverseOptions m, MonadIO m, MonadLogger m)
type Traversing m = (OptsIO m, MonadUnliftIO m)


-- ############################################
-- ####  Traversing fold
-- ############################################

traverseBakTree :: Traversing m
                => Path -- ^ the destination directory
                -> Maybe Path -- ^ the src directory
                -> (Path -> PathNode -> m ()) -- ^ the action which uses src and dest paths
                -> BakTree -> m ()
traverseBakTree oldDestFolder currentSrcBaseDir copyFun (BakTree dirSegments newBase paths subs) = 
  let -- | appends new to oldbase 
      appendToBase Nothing new = new
      appendToBase oldBase Nothing = oldBase
      appendToBase _ n@(Just (Path Abs _)) = n
      appendToBase (Just oldBase) (Just (Path Rel val)) = Just $ over rawPath (`mappend` ("/" <> val)) oldBase  

      newDestFolder = appendSegment (T.intercalate "/" dirSegments) oldDestFolder
      newSrcBaseDir = currentSrcBaseDir `appendToBase` newBase

      addBase Nothing p = p
      addBase _ p@(Path Abs _) = p
      addBase (Just (Path Abs b))  (Path Rel p) = Path Abs (b <> "/" <> p)
      addBase (Just (Path Rel _))  _ = panic "base is relative"

      sourcesWithBase =  (over (inputPath % path) (addBase newSrcBaseDir) <$> paths)


      -- parallelity helper definitions
      parallelSources = filter ((Par ==) . _modifier ) sourcesWithBase
      sequentialSources = filter (not . (Par ==) . _modifier) sourcesWithBase

  in  do
    newDestFolder <- createDirsDeep oldDestFolder dirSegments
    if null parallelSources
      then do
        for_ sequentialSources $ copyFun newDestFolder
        for_ subs $ traverseBakTree newDestFolder newSrcBaseDir copyFun
      else withRunInIO $ \runInIO -> do
        parallel_ 
          [ runInIO $ do
              for_ sequentialSources $ copyFun newDestFolder
              for_ subs $ traverseBakTree newDestFolder newSrcBaseDir copyFun
          , parallel_ $ runInIO . copyFun newDestFolder <$> parallelSources 
          ]
      
{-# INLINABLE createDirsDeep #-}
createDirsDeep base [] = pure base
createDirsDeep base (headSegment:rest) = 
  let newBase = appendSegment headSegment base
  in do
    unlessM (asks restore <||> asks dryRun) 
      $ mkDir newBase
    log "mkDir" "creating directory" $ Just newBase
    createDirsDeep newBase rest

-- ############################################
-- ####  Some Traversers
-- ############################################

simpleTraverser :: Traversing m => Path -> BakTree -> m ()
simpleTraverser dest = traverseBakTree dest Nothing actOnPaths

actOnPaths :: Traversing m => Path -> PathNode -> m ()
actOnPaths destPath (PathNode (InputPath srcPath options) modif) = do
  unlessM (pathIsExist srcPath <||> asks restore) 
    $ traverseErr srcPath PathDoesNotExist 

  -- choosing the action to perform on paths
  restoring <- asks restore
  let action = case modif of
            Link | not restoring -> doLink  
            _    |     restoring -> doRestore
                 | otherwise     -> doCopy
          
  handle keepGoingOnException $ case options of
    Regex regex -> do
      unlessM ( pathIsDir srcPath ) -- can only match in directories
        $ traverseErrMsg srcPath NotADirectory "Can only use regular expressions in directories"
      files <- withPath srcPath listDirectory
      for_ files $ \f -> 
        when (f `matches` regex)
          $ ( action  `on` appendSegment (T.pack f) ) srcPath destPath

    NewName newNameSegment ->
      action srcPath $ appendSegment newNameSegment destPath

    RunCommand filename command -> do
      unlessM (pathIsDir srcPath) 
        $ traverseErrMsg srcPath NotADirectory $ "Path must be directory since it is used as base directory for command `" <> command <> "`"
      let writeCommandToFile handle = (Process.shell $ T.unpack command) 
            { Process.cwd = Just . T.unpack . view raw $ srcPath
            , Process.std_out = Process.UseHandle handle  
            }
          destFile = view raw $ appendSegment filename destPath
      logOtherN (LevelOther "command") $ "writing output of command `" <> command <> "` to " <> destFile
      _ <- liftIO $ withFile (T.unpack destFile) WriteMode (\h -> Process.createProcess (writeCommandToFile h))
      return ()

    NoOptions -> 
      action srcPath $ appendSegment ( localName srcPath ) destPath



-- ############################################
-- ####  helper functions
-- ############################################


doLink :: Traversing m => Path -> Path -> m ()
doLink target dest = do
  logOtherN (LevelOther "linking") $ show $ "creating link to" <+> pretty target <+> "from" <+> pretty dest
  unlessM (asks dryRun) $ fileKind target >>= liftIO . \case
    IsDir -> ( createDirectoryLink `on` (T.unpack . view raw) ) target dest
    _     -> ( createFileLink `on` (T.unpack . view raw) ) target dest


doCopy :: Traversing m => Path -> Path -> m ()
doCopy src dest = do
  isSymbolic <- pathIsLink src
  boolM (pure isSymbolic <&&> asks ignoreAllLinks)
    ( log "ignoring" "link" $ Just src )
    $ do
    realSrc <- if isSymbolic
      then do
        src' <- resolveLink src
        unlessM (pathIsExist src' <||> asks ignoreBrokenLinks) 
          $ traverseErrMsg src BrokenLink $ "target of link does not exist: " <> src'^.raw
        return src'
      else pure src
    
    kind <- fileKind realSrc
    cpFunctionForKindOf kind realSrc dest 


doRestore :: Traversing m => Path -> Path -> m ()
doRestore src dest = do
  kind <- fileKind dest
  whenM (pathIsExist src) $ do
    log "removing" "path" $ Just src
    unlessM ( asks dryRun )
      $ withPath src removePathForcibly
  cpFunctionForKindOf kind dest src



cpFunctionForKindOf :: Traversing m => PathKind -> ( Path -> Path -> m () )
cpFunctionForKindOf kind = \src dst -> do
    logCopy src dst
    unlessM ( asks dryRun ) $ ( case kind of
              IsDir  -> cpDirRec
              IsFile -> cpFile
              IsLink -> cpFile
              IsNonExistent -> \s _ -> do
                whenM (permissionDenied src) 
                  $ traverseErrMsg src PermissionDenied "You probably need to run the backup as sudo"
                asks ignoreBrokenLinks >>= \case
                  True -> log "ignoring" "broken link" $ Just src
                  False -> traverseErrMsg s PathDoesNotExist "Could not resolve link") src dst

cpFile :: MonadIO m => Path -> Path -> m ()
cpFile src dest = do
  liftIO $ (copyFile `on` T.unpack . _rawPath) src dest

cpDirRec :: Traversing m => Path -> Path -> m ()
cpDirRec src dest = do
  mkDir dest
  entries <- liftIO $ withPath src listDirectory
  for_ (T.pack <$> entries) $ \entry -> do
    (doCopy `on` appendSegment entry) src dest


mkDir :: OptsIO m => Path -> m ()
mkDir dir = do
  log "mkDir" "creating directory" $ Just dir
  unlessM (asks dryRun) $ withPath dir createDirectory


-- ############################################
-- ####  helper with exceptions
-- ############################################


keepGoingOnException :: OptsIO m => TraverseError -> m ()
keepGoingOnException e = asks keepGoing >>= bool 
        (throwIO e) 
        (liftIO $ printf "[Ignoring] Ignoring exception: %s\n" $ showt $ pretty e)


permissionDenied :: MonadUnliftIO m => Path -> m Bool
permissionDenied src = do
  tryIO (void $ withPath src getPermissions) >>= return . either isPermissionError (const False)


matches :: String -> Regex -> Bool
matches s r = isJust $ matchRegex r s

boolM :: Monad m => m Bool -> m a -> m a -> m a
boolM t x y = do
  b <- t
  if b 
    then x 
    else y

