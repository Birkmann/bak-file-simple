{-# LANGUAGE  BangPatterns, TemplateHaskell, NoMonomorphismRestriction, OverloadedStrings, MultiWayIf, LambdaCase, ScopedTypeVariables #-}
module PathHelper where

-- import Prelude
import Protolude hiding ((%), log)
import qualified GHC.Show as Show(Show(..))

import qualified Data.Text as T
import Text.Regex
import Optics.TH
import Optics
import UnliftIO.Directory
import System.Directory(getSymbolicLinkTarget)
import Data.Maybe(fromJust)
import Data.Text.Prettyprint.Doc

-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
-- --------------------------------------------
-- ###  Types
-- --------------------------------------------
-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

{-# INLINE showt #-}
showt :: Show a => a -> T.Text 
showt = show @_ @T.Text

-- ############################################
-- ####  General 
-- ############################################

type DirSegment = T.Text
type RawPath = T.Text

class GetRawPath a where
  raw :: Lens' a RawPath


data PathKind = IsFile | IsDir | IsLink | IsNonExistent deriving (Show, Eq, Ord, Enum, Read)

-- ############################################
-- ####  Path
-- ############################################

-- | Invariant: no multiple slashes, and a Path never ends with a slash
data Path = Path { _base :: !Base, _rawPath :: !T.Text } deriving (Show, Eq)
data Base = Rel | Abs deriving (Show, Eq, Read, Enum)
makeLenses ''Path
makePrisms ''Base

instance GetRawPath Path where
  raw = rawPath

instance Pretty Path where
  pretty = pretty . view raw



{-# INLINABLE localName #-}
localName :: Path -> T.Text
localName (Path Abs p) = T.takeWhileEnd (/= '/') p
localName (Path Rel p) = 
  let lastSeg = T.takeWhileEnd (/= '/') p
  in if T.null lastSeg  
        then p
        else lastSeg

sanitizePath :: RawPath -> Path
sanitizePath p =
  let san = remDuplicateSlashes p
      b
        | T.head san == '/' = Abs
        | otherwise = Rel
      remDuplicateSlashes :: T.Text -> T.Text
      remDuplicateSlashes = fst . T.foldl'  f ("", False)
      f (!t, !lastWasSlash) '/' = if lastWasSlash
                                    then (t, True)
                                    else (t `T.snoc` '/', True)
      f (!t, _) c = (t `T.snoc` c, False)
  in Path b $ case fromJust . T.unsnoc $ san of
      ("", _) -> san -- inputPath consists only of one character
      (s, '/') -> s
      _ -> san

appendSegment :: DirSegment -> Path -> Path
appendSegment seg
  | T.head seg == '/' || T.last seg == '/' = panic  "internal error: path segment starts or ends with slash"
  | otherwise = over rawPath (<> ("/" <> seg))



-- ############################################
-- ####  Input Path
-- ############################################

type NewName = T.Text

data PathOptions 
  = Regex   Regex  -- ^ Copy all paths that match Regex
  | NewName NewName -- ^ Rename copied path to new name
  | RunCommand NewName T.Text -- ^ Write output from command T.Text to file NewName with working directory src
  | NoOptions
  
makePrisms ''PathOptions

instance Show PathOptions where
  show (Regex _) = "Regex <REGEX>"
  show (NewName name) = "NewName " <> show name
  show (RunCommand filename command) = "RunCommand " <> show command <> " " <> show filename
  show (NoOptions) = "NoOptions"

data InputPath
    = InputPath { _path :: !Path, _inputOpts ::  !PathOptions } deriving (Show)
makeLenses ''InputPath



instance GetRawPath InputPath where
  raw = path % raw

instance Pretty InputPath where
  pretty = pretty . view raw


-- ############################################
-- ####  PathNode
-- ############################################

data PathNode = PathNode
  { _inputPath :: !InputPath
  , _modifier :: Modifier
  } deriving (Show)



-- | Modifier for nodes: Plus = add
data Modifier 
  = Plus
  | Link
  | Par deriving (Show, Eq, Enum, Ord)
  
makeLenses ''Modifier
makeLenses ''PathNode

instance GetRawPath PathNode where
  raw = inputPath % raw

instance Pretty PathNode where
  pretty = pretty . view raw







-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
-- --------------------------------------------
-- ###  Helper
-- --------------------------------------------
-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


testPath :: MonadIO io =>  (FilePath -> IO Bool) -> T.Text -> io Bool
testPath f = liftIO . f . T.unpack

isExist, isFile, isDir ::  MonadIO io => T.Text -> io Bool
[isExist, isFile, isDir] = testPath <$> [doesPathExist, doesFileExist, doesDirectoryExist]

{-# INLINABLE withPath #-}
withPath :: MonadIO io => Path -> (FilePath -> IO a) -> io a
withPath p f = liftIO . f . T.unpack . _rawPath $ p

pathIsExist, pathIsFile, pathIsDir, pathIsLink :: MonadIO io => Path -> io Bool
[pathIsExist, pathIsFile, pathIsDir, pathIsLink] =  flip withPath <$> [doesPathExist, doesFileExist, doesDirectoryExist, myPathIsSymbolicLink]

myPathIsSymbolicLink :: MonadIO io => FilePath -> io Bool
myPathIsSymbolicLink f = liftIO $ pathIsSymbolicLink f `catch` (\(_ :: IOException) -> return False)

fileKind :: MonadIO io => Path -> io PathKind
fileKind p = do
  isf <- pathIsFile p
  isd <- pathIsDir p
  isl <- pathIsLink p
  return $ if
    | isf -> IsFile
    | isd -> IsDir
    | isl -> IsLink
    | otherwise -> IsNonExistent


resolveLink :: MonadIO io => Path -> io Path
resolveLink src = do
  target <- withPath src getSymbolicLinkTarget
  liftIO . fmap (sanitizePath . T.pack) . makeAbsolute $ target


mkAbs :: MonadIO io => Path -> io Path
mkAbs p = sanitizePath . T.pack <$> withPath p makeAbsolute

