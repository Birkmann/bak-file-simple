# Changelog for bak-file-simple

## [0.1.0.1] - 2020-03-03
### Added restore option
    - With --restore (short -R) one can restore all files from the output directory
    - Restoring DELETES ALL CHANGES made in ANY of the files that were backed up to the output directory
    - Restoring ignores links
