module Main where

import Protolude

import CLI

main :: IO ()
main = parseUserOpts >>= runBackupWithConfig
